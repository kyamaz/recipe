- create project
  run [name_service (in docker-compose-compose)]
  `docker-compose run api sh -c "django-admin startproject app ."`
- test
  `docker-compose run --rm api sh -c "python3 manage.py test && flake8"`
- create addt
  `docker-compose run --rm api sh -c "django-admin startapp [NAME]"`
- migration
  `docker-compose run api sh -c "python3 manage.py makemigrations [APP]"`
  `docker-compose run api sh -c "python3 manage.py runmigration"`
- add app
