from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Ingredient, Recipe
from recipe.serializers import IngredientSerializer

import tempfile
import os
from PIL import Image

INGREDIENTS_URL = reverse('recipe:ingredient-list')


def image_url_id(recipe_id):
    return reverse('recipe:recipe-upload-image', args=[recipe_id])


class TestPublicIngredient(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        res = self.client.get(INGREDIENTS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class TestPrivateIngredient(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user('test&test.com', 'ttttttt')
        self.client.force_authenticate(self.user)

    def test_get_list(self):
        Ingredient.objects.create(user=self.user, name="test_i")
        Ingredient.objects.create(user=self.user, name="test_J")

        res = self.client.get(INGREDIENTS_URL)

        ingredients = Ingredient.objects.all().order_by('-name')
        serializer = IngredientSerializer(ingredients, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_list_by_user(self):
        user2 = get_user_model().objects.create_user('test1&test.com', '11111')
        Ingredient.objects.create(user=user2, name="test_2")
        ingredients = Ingredient.objects.create(user=self.user, name="test_i")
        res = self.client.get(INGREDIENTS_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], ingredients.name)

    def test_create_successful(self):
        payload = {'name': 'test'}
        self.client.post(INGREDIENTS_URL, payload)
        exists = Ingredient.objects.filter(user=self.user, name=payload['name']).exists()
        self.assertTrue(exists)

    def test_create_invalid(self):
        payload = {'name': ''}
        res = self.client.post(INGREDIENTS_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_ingredients_assigned_to_recipes(self):
        ingredient1 = Ingredient.objects.create(
            user=self.user,
            name='a'
        )
        ingredient2 = Ingredient.objects.create(
            user=self.user,
            name='b'
        )
        recipe = Recipe.objects.create(
            title='ar',
            time_minutes=5,
            price=10.00,
            user=self.user
        )
        recipe.ingredients.add(ingredient1)

        res = self.client.get(
            INGREDIENTS_URL,
            {'assigned_only': 1}
        )
        serializer1 = IngredientSerializer(ingredient1)
        serializer2 = IngredientSerializer(ingredient2)
        self.assertIn(serializer1.data, res.data)
        self.assertNotIn(serializer2.data, res.data)

    def test_retrieve_ingredient_assigned_unique(self):
        ingredient = Ingredient.objects.create(user=self.user, name='a')
        Ingredient.objects.create(user=self.user, name='b')
        recipe1 = Recipe.objects.create(
            title='ra',
            time_minutes=30,
            price=12.00,
            user=self.user
        )

        recipe1.ingredients.add(ingredient)
        recipe2 = Recipe.objects.create(
            title='rb',
            time_minutes=30,
            price=12.00,
            user=self.user
        )
        recipe2.ingredients.add(ingredient)
        res = self.client.get(
            INGREDIENTS_URL,
            {'assigned_only': 1}
        )

        self.assertEqual(len(res.data), 1)


class RecipeImageTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user('user', 'tespass')
        self.client.force_authenticate(self.user)
        self.recipe = sample_recipe(self.user)
