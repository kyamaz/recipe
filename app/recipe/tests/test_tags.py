from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from core.models import Tag, Recipe

from recipe.serializers import TagSerializer


TAGS_URL = reverse('recipe:tag-list')


class TestPublicTags(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        res = self.client.get(TAGS_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class TestPrivateTags(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user('test@test.com', 'Azertyu')
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_get_tags(self):
        Tag.objects.create(user=self.user, name="Test")
        Tag.objects.create(user=self.user, name="Desserty")

        res = self.client.get(TAGS_URL)
        tags = Tag.objects.all().order_by('-name')
        serializer = TagSerializer(tags, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        serializer = TagSerializer(tags, many=True)
        self.assertEqual(res.data, serializer.data)

    def test_get_user_tags(self):
        USER2 = get_user_model().objects.create_user('test1@test1.com', '1Azertyu')
        Tag.objects.create(user=USER2, name="other")
        tag = Tag.objects.create(user=self.user, name="mine")

        res = self.client.get(TAGS_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], tag.name)

    def test_create_tag_success(self):
        payload = {'name': 'test'}
        self.client.post(TAGS_URL, payload)
        exists = Tag.objects.filter(
            user=self.user,
            name=payload['name']
        )
        self.assertTrue(exists)

    def test_create_tag_invalid(self):
        payload = {'name': ''}
        res = self.client.post(TAGS_URL, payload)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_tags_assigned_recipes(self):
        tags1 = Tag.objects.create(user=self.user, name='a')
        tags2 = Tag.objects.create(user=self.user, name='b')
        recipe = Recipe.objects.create(
            title='ab',
            time_minutes=10,
            price=5.00,
            user=self.user,
        )
        recipe.tags.add(tags1)
        res = self.client.get(
            TAGS_URL,
            {'assigned_only': 1}
        )
        serializer1 = TagSerializer(tags1)
        serializer2 = TagSerializer(tags2)

        self.assertIn(serializer1.data, res.data)
        self.assertNotIn(serializer2.data, res.data)

    def test_retrieve_tags_assigned_unique(self):
        tag = Tag.objects.create(user=self.user, name='a')
        Tag.objects.create(user=self.user, name='b')
        recipe1 = Recipe.objects.create(
            title='at',
            time_minutes=10,
            price=5.00,
            user=self.user,
        )

        recipe1.tags.add(tag)
        recipe2 = Recipe.objects.create(
            title='ab',
            time_minutes=10,
            price=5.00,
            user=self.user,
        )
        recipe2.tags.add(tag)

        res = self.client.get(
            TAGS_URL,
            {'assigned_only': 1}
        )
        self.assertEqual(len(res.data), 1)
