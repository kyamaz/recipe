from django.contrib.auth import get_user_model
from django.test import TestCase
from core import models
from unittest.mock import patch


def sample_user(email="test@test.com", password="passwordpassword"):
    return get_user_model().objects.create_user(email, password)


class ModelTest(TestCase):
    def test_create_user_with_email_successful(self):
        email = "test@londonappdev.com"
        password = "Password123"
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        email = "test@LONDONNNN.com"
        user = get_user_model().objects.create_user(email, "PPPPPPP")

        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, "PPPPPPP")

    def test_super_user_is_created(self):
        user = get_user_model().objects.create_superuser(
            "est@londonappdev.com",
            "supermoredepase"
        )
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    def test_tag_str(self):
        tag = models.Tag.objects.create(
            user=sample_user(),
            name='test'
        )
        self.assertEqual(tag.name, str(tag))

    def test_ingredient_str(self):
        ingredient = models.Ingredient.objects.create(
            user=sample_user(),
            name="test"
        )
        self.assertEqual(ingredient.name, str(ingredient))

    def test_recipe_str(self):
        recipe = models.Recipe.objects.create(
            user=sample_user(),
            title="test",
            time_minutes=5,
            price=5.00

        )
        self.assertEqual(str(recipe), recipe.title)

    @patch('uuid.uuid4')
    def test_file_uuid(self, mock_uuid):
        uuid = 'uuid-test'
        mock_uuid.return_value = uuid
        file_path = models.recipe_img_file_path(None, 'my-image.jpg')

        exp_path = f'uploads/recipe/{uuid}.jpg'
        self.assertEqual(file_path, exp_path)
