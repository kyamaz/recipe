from django.test import TestCase
from app.calc import add, sub


class AddTest(TestCase):
    def test_add_num(self):
        """ test """
        self.assertEqual(add(1, 2), 3)

    def test_sub_num(self):
        """  val substract"""
        self.assertEqual(sub(3, 1), 2)
